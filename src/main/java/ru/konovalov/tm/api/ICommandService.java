package ru.konovalov.tm.api;

import ru.konovalov.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
