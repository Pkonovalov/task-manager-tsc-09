package ru.konovalov.tm.service;

import ru.konovalov.tm.api.ICommandRepository;
import ru.konovalov.tm.api.ICommandService;
import ru.konovalov.tm.model.Command;
import ru.konovalov.tm.repository.CommandRepository;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
